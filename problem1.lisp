;; URL: https://projecteuler.net/problem=1
;; Description:
;; If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
;; Find the sum of all the multiples of 3 or 5 below 1000.

(defun multiple-of-3-or-5 (num)
  "Returns num if it is a multiple of three or five. Otherwise, it returns 0."
  (if (or (eq (mod num 3) 0)
          (eq (mod num 5) 0))
      num
      0))

;;; Loop from 1 to 999. Sum the result of (multiple-or-3-or-5 i) into a variable
;;; named 'sum'. Print the sum when done.
(loop for i from 1 to 999
   summing (multiple-of-3-or-5 i) into sum
   finally
     (format t "The sum is: ~a~%" sum))
